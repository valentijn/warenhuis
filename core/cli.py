"""CLI frontend for cli.py"""
import sys
import traceback
import MySQLdb
import config # project specific
import core   # project specific

def get_help():
    """Display help for the commands"""
    text = """get:    Haal de status van een item op.
insert: Voeg een item toe aan de databasea
remove: Verwijder een item uit de database
give:   Zet de status van een item naar meegenomoen
take:   Zet de status van een item naar terug en verwijder alle andere metadata
swdb:   Gebruik een andere database"
exit:   Sluit het programma."""

    print(text)

def database_connect(server):
    """Connect to one of the databases specified in the config file"""
    global table, db, cursor

    try:
        host = config.get_value(server, 'ip')
        user = config.get_value(server, 'user')
        table = config.get_value(server, 'table')
        database = config.get_value(server, 'database')
        password = config.get_value(server, 'password')
    except:
        print("Ik kan niet de waarde een van de variabelen vinden.")

    try:
        db = MySQLdb.connect(host=host, user=user, passwd=password, db=database)
        cursor = db.cursor()
    except:
        print("Ik kan geen connectie maken met de database.")
        print(traceback.print_exc()) # For debugging
        sys.exit(1)

database_connect(config.get_value('default', 'server'))


print("Welkom bij de Nova College item database.")
print("============================================")
print("Voor hulp enter de command help")

while True:
    # ensure sane input by making the input lowercase and removing any trailing white space
    user_input = input("\nEnter Commando: ").lower().strip() 

    # get use input
    if user_input == "get":
        core.get_status_database(cursor, table, input("Voer het ID in van de item: "))

        query = cursor.fetchone()

        if query == None:
            continue
        elif query[5] == 1:
            print("ID: {0}\nAannemer: {1}\nGever: {2}\nSoort: {3}\nDatum: {4}\n" \
                  "Status: meegenomen".format(query[0], query[1], query[2], query[3], query[4]))
        else:
            print("De item met het ID {} is er nog".format(query[0]))

    elif user_input == "insert":
        core.insert_into_database(cursor, table, input("Soort: "))
    elif user_input == "remove":
        core.alter_table_database(cursor, table, input("ID: "), input("Soort: "))
    elif user_input == "give":
        core.give_item_database(cursor, table, input("ID: "), input("Aannemer: "), input("Gever: "))
    elif user_input == "take":
        core.return_item_database(cursor, table, input("ID: "))
    elif user_input == "sort":
        attribute = input("Enter an attribute: ").lower()
        if attribute == "taken":
            value = input("Value: ").lower()
            if value == "true":
                core.filter_database(cursor, table, attribute, 1)
            else:
                core.filter_database(cursor, table, attribute, 0)
        else:
            core.filter_database(cursor, table, attribute, input("Value: "))

        row = cursor.fetchone()
        while row is not None:
            if row[5] == 1:
                status = "true"
            else:
                status = "false"

            print("ID: {0}\nSoort: {1}\nAannemer: {2}\nGiver: {3}\nDatum: {4}\nStatus: {5}\n" \
                  .format(row[0], row[1], row[2], row[3], row[4], status))

            row = cursor.fetchone()

    elif user_input == "swdb":
        database_connect(input("Database: "))
    elif user_input == "help":
        get_help()
    elif user_input == "exit":
        sys.exit(0)
    elif user_input != "":
        print("De commando '{}' is niet gevonden".format(user_input))

    db.commit()
