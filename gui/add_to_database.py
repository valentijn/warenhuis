# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'add_to_database.ui'
#
# Created by: PyQt5 UI code generator 5.6.1.dev1604271126
#
# WARNING! All changes made in this file will be lost!

import core
from gui import dialog
from PyQt5 import QtCore, QtGui, QtWidgets

class AddDatabase(dialog.Dialog):
    """Inherent the dialog class and connect it to an action"""
    def set_action(self):
        self.buttonBox.accepted.connect(self.add_to_database) # run function on button click

    def add_to_database(self):
        if not self.get_values():
            return
        
        core.core.insert_into_database(self.cursor, self.table, self.Soort.text(),
                                       self.ID.text(), self.student, self.werknemer)
        self.db.commit()
        self.Dialog.close()
