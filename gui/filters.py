# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'filters.ui'
#
# Created by: PyQt5 UI code generator 5.6.1.dev1604271126
#
# WARNING! All changes made in this file will be lost!

import core
import signal
import traceback
from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def __init__(self, main_window, cursor, table):
        self.window = main_window
        self.cursor = cursor
        self.table = table

    def setupUi(self, Dialog):
        signal.signal(signal.SIGINT, signal.SIG_DFL) # listen to SIGKILL
        self.dialog = Dialog
        self.dialog.resize(500, 180)

        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.comboBox = QtWidgets.QComboBox(Dialog)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)

        # create a dropdown menu with values
        self.comboBox.addItems(["ID", "Student", "Werknemer", "Soort", "Status", "Datum"])
        self.comboBox.setItemData(0, "id")
        self.comboBox.setItemData(1, "taker")
        self.comboBox.setItemData(2, "giver")
        self.comboBox.setItemData(3, "kind")
        self.comboBox.setItemData(4, "taken")
        self.comboBox.setItemData(5, "date")

        self.comboBox.currentIndexChanged.connect(self.setup)
        self.gridLayout.addWidget(self.comboBox, 0, 0, 1, 1)

        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.gridLayout.addWidget(self.buttonBox, 1, 0, 1, 2)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(self.run)
        self.buttonBox.rejected.connect(Dialog.reject)
        self.setup()

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))

    def setup(self):
        item = self.comboBox.itemData(self.comboBox.currentIndex()) # get the current item
        try:
            # remove the widget
            self.gridLayout.removeWidget(self.listView)
            self.listView.deleteLater()
            self.listView = None
        except: pass

        try:
            self.gridLayout.removeWidget(self.lineEdit)
            self.lineEdit.deleteLater()
            self.lineEdit = None
        except: pass
        
        try:
            self.gridLayout.removeWidget(self.newBox)
            self.newBox.deleteLater()
            self.newBox = None
        except: pass

        if item == "taken":
            self.newBox = QtWidgets.QComboBox(self.dialog)
            self.newBox.addItems(["Uitgeleend", "In Magazijn"])
            self.newBox.setItemData(0, "1")
            self.newBox.setItemData(1, "0")
            self.gridLayout.addWidget(self.newBox, 0, 1, 1, 1)
        else:
            self.lineEdit = QtWidgets.QLineEdit(self.dialog)
            self.gridLayout.addWidget(self.lineEdit, 0, 1, 1, 1)


    def run(self):
        self.listView = QtWidgets.QListView(self.dialog)
        self.gridLayout.addWidget(self.listView, 2, 0, -1, 0)
        self.model = QtGui.QStandardItemModel()
        item = self.comboBox.itemData(self.comboBox.currentIndex())

        if item == "taken":
            core.core.filter_database(self.cursor, self.table, item,
                                      self.newBox.itemData(self.newBox.currentIndex()))
        else:
            core.core.filter_database(self.cursor, self.table, item, self.lineEdit.text())

        row = self.cursor.fetchone()
        self.model.appendRow(QtGui.QStandardItem(
            "ID:\tSoort:\tStudent:\tWerknemr:\tDatum:\tStatus:"))

        # add a line for each row in the SQL database
        while row is not None:
            self.model.appendRow(QtGui.QStandardItem("{0}\t{1}\t{2}\t{3}\t{4}\t{5}"
                                                     .format(row[0], row[1],
                                                             row[2], row[4], row[5],
                                                             ("Uitgeleend" if row[6] == 1
                                                              else "In Magazijn"))))
            row = self.cursor.fetchone()

        self.listView.setModel(self.model)
