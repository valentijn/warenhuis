* Core
** DONE Add a getter, setter and cleaner
** DONE Write a gui frontend for the basic functionality
** DONE A notification when an item has been away for a long time
** DONE View all button (maybe filters)
** DONE Config file
** DONE Sort by person
** DONE Add item
** DONE List all IDs of the category X
** DONE Remove item
** DONE Modify item
** TODO better password mechanism (offlineimap?)
** DONE runtime database switching
** TODO Keeping track of broken equipment and offenders

* GUI
** DONE QT frontend
** DONE QT4 -> QT5
** DONE update to the new(er) api
** DONE integrate CLI -> GUI
** DONE Hanging given away window
** TODO pick first ID from category X that is not taken
** DONE Rename 'nmerer' and 'gever'
** TODO Extra filters

* CLI
** TODO Scripting functionality
** TODO To lower the item names
   
