from gui import dialog
from PyQt5 import QtWidgets
import core
import sys

class Modify(dialog.Dialog):
    """Test"""
    def set_action(self):
        self.buttonBox.accepted.connect(self.modify_database)
        
    def modify_database(self):
        if self.Soort.text() == "":
            QtWidgets.QMessageBox.critical(self.window, "Error!",
                                           "Niet alle velden zijn ingevuld.")
            return
            
        core.core.alter_table_database(self.cursor, self.table, self.ID.text(), self.Soort.text())
        self.db.commit()
        self.Dialog.close()
