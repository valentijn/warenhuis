import signal
import core
from PyQt5 import QtCore, QtGui, QtWidgets

class Dialog(object):
    def __init__(self, cursor, table, database, main_window):
        self.cursor = cursor
        self.table = table
        self.db = database
        self.window = main_window

    def setup_ui(self, Dialog):
        signal.signal(signal.SIGINT, signal.SIG_DFL) # listen to SIGKILL
        Dialog.setObjectName("Dialog")
        Dialog.resize(492, 111)
        self.Dialog = Dialog

        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.Werknemer = QtWidgets.QLineEdit(Dialog)
        self.Soort = QtWidgets.QLineEdit(Dialog)
        self.ID = QtWidgets.QLineEdit(Dialog)
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_4 = QtWidgets.QLabel(Dialog)
        self.label = QtWidgets.QLabel(Dialog)
        self.Student = QtWidgets.QLineEdit(Dialog)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)

        self.gridLayout.addWidget(self.label_2, 0, 2, 1, 1)
        self.gridLayout.addWidget(self.Werknemer, 1, 3, 1, 1)
        self.gridLayout.addWidget(self.Soort, 0, 3, 1, 1)
        self.gridLayout.addWidget(self.ID, 0, 1, 1, 1)
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)
        self.gridLayout.addWidget(self.label_4, 1, 2, 1, 1)
        self.gridLayout.addWidget(self.Student, 1, 1, 1, 1)

        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.set_action() # allow customization of actions on inherentence
        self.buttonBox.setCenterButtons(True)
        self.gridLayout.addWidget(self.buttonBox, 3, 0, 1, 4)
        self.retranslateUi(Dialog)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

        Dialog.setTabOrder(self.ID, self.Soort)
        Dialog.setTabOrder(self.Soort, self.Student)
        Dialog.setTabOrder(self.Student, self.Werknemer)

    def get_values(self):
        if self.Soort.text() == "":
            QtWidgets.QMessageBox.critical(self.window, "Error!", "Niet alle velden zijn ingevuld!")
            return False
        else:
            if not self.Student.text():
                self.student = "Null"
            else:
                self.student = self.Student.text()

            if not self.Werknemer.text():
                self.werknemer = "Null"
            else:
                self.werknemer = self.Student.text()

            return True

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Add to Database"))
        self.label_2.setText(_translate("Dialog", "Soort*"))
        self.label.setText(_translate("Dialog", "Product ID*"))
        self.label_3.setText(_translate("Dialog", "Student"))
        self.label_4.setText(_translate("Dialog", "Werknemer"))
