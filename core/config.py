"""Load a the configuration file"""
import pathlib
from configparser import SafeConfigParser
from os import getcwd
from os.path import expanduser

USER_HOME = expanduser("~") # get the home directory
CONFIG_PATHS = [
    USER_HOME + '/.config/warenhuis/config.ini',
    USER_HOME + '/.config/warenhuis.ini',
    USER_HOME + '/.warenhuis/config.ini',
    USER_HOME + '/.warenhuis.ini',
    getcwd() + '/config.ini'
]

# Parse the first found configuration file
config = SafeConfigParser()
for configfile in CONFIG_PATHS:
    if pathlib.Path(configfile).exists():
        config.read(configfile)
        break

def get_value(table, row):
    """Get the value from the configuration file"""
    return config[table][row]

