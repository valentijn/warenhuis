# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'change_database.ui'
#
# Created by: PyQt5 UI code generator 5.6.1.dev1604271126
#
# WARNING! All changes made in this file will be lost!

import core
from PyQt5 import QtCore, QtGui, QtWidgets, Qt
from random import randint

class Ui_Dialog(object):
    def __init__(self, root_window, dialog):
        self.window = root_window
        self.dialog = dialog
        
    def setupUi(self, Dialog):
        self.Dialog = Dialog
        self.Dialog.setObjectName("Dialog")
        self.Dialog.resize(300, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.listView = QtWidgets.QListView(Dialog)
        self.verticalLayout.addWidget(self.listView)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(self.do_something_i_dont_know)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        self.model = QtGui.QStandardItemModel()

        # create a list of all the available databases
        for x in core.config.config:
            if x.upper() != "DEFAULT":
                self.model.appendRow(QtGui.QStandardItem("{0}\t\t{1}"\
                                                         .format(x, core.config.config[x]['ip'])))

        self.listView.setModel(self.model)

    def do_something_i_dont_know(self):
        try:
            item = self.model.itemFromIndex(self.listView.selectedIndexes()[0])
            self.dialog.init_database(str(item.text().split()[0]))
            self.Dialog.close()
        except IndexError:
            pass

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
