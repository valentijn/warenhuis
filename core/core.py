""" The core implementation of the program which mostly exists out of SQL statements.

Functions:
----------
give_item_database: change item information to that of the person who loaned it.
return_item_database: change the information of a item back.
get_status_database: return the information of a item
insert_into_database: add an item to the database
alter_table_database: change the kind of the item
filter_database: return information of every item fitting a specific query.

Variables:
----------
cursor: the database.
table: the table in the database.
item_id: the id of the item.
taker: the name of the person who loans the item.
giver: the name of the person who loaned it out.
kind: the type of item.
field_type: the column you want to filter on.
value: the value of the column you want to filter.
"""
def give_item_database(cursor, table, item_id, taker, giver):
    return_item_database(cursor, table, item_id)
    """ Alters the table the information of the person who loaned it. """
    cursor.execute("UPDATE {0} SET taker='{1}', giver='{2}',"
                   "date=CURDATE(), taken=true WHERE id='{3}';"
                   .format(table, taker, giver, item_id))

def return_item_database(cursor, table, item_id):
    cursor.execute("SELECT taker FROM {0} WHERE id='{1}';".format(table, item_id))
    old_taker = cursor.fetchone()[0]

    if old_taker is None:
        cursor.execute("SELECT old_taker FROM {0} WHERE ID='{1}';".format(table, item_id))
        old_taker = cursor.fetchone()[0]

    """ Clean the SQL by removing the custom information."""
    cursor.execute("UPDATE {0} SET taker=Null, giver=Null, date=Null,"
    "taken=false, old_taker='{1}' WHERE id='{2}'".format(table, old_taker,
                                                         item_id))

def get_status_database(cursor, table, item_id):
    """ Get the information for the table with the id ITEM_ID. """
    cursor.execute("SELECT * FROM {0} WHERE id='{1}'".format(table, item_id))

def insert_into_database(cursor, table, kind, item_id, giver="Null", taker="Null"):
    """ Add an item to the database. """
    if taker == "Null" and giver == "null":
        cursor.execute("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', 'Null', CURDATE(),"
                       "true);".format(table, item_id, kind, giver, taker))
    else:
        cursor.execute("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', 'Null', CURDATE(),"
                       "false);".format(table, item_id, kind, giver, taker))

def alter_table_database(cursor, table, item_id, kind):
    """ Alter the database item, this is in place of a 'remove' function since that would
    be to detructive to the AUTO_ID system """
    cursor.execute("UPDATE {0} SET kind='{1}' WHERE id='{2}'"
                   .format(table, kind, item_id))

def filter_database(cursor, table, field_type, value):
    """ Filter the database on FIELD_TYPE with the value VALUE """
    cursor.execute("SELECT * FROM {0} WHERE {1}='{2}'".format(table, field_type, value))
