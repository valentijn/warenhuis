* Requirements for compiling
- Python 3
- [[https://pypi.python.org/pypi/mysqlclient/1.3.7][mysqlclient]]
- [[https://riverbankcomputing.com/software/pyqt/download5][PyQt5]]
- MySQL

* Install
- Download [[https://www.python.org/][Python 3.5]] (already installed in Linux)
- Ensure the default server in config.ini points to the right place
- run

* Compile
** Windows
- Install Python3.5
- Get pyinstaller and pyqt5 from pip
- Download mysqlclient from this [[https://www.lfd.uci.edu/~gohlke/pythonlibs/#mysqlclient][this link.]]
- Open cmd.exe and go to the path where the program is stored
#+BEGIN_SRC sh
 pyinstaller.exe --onefile --windowed run.py 
#+END_SRC
- Copy run.exe over from dist to the root of the directory
- Remove dist/ and build/

** GNU/Linux
- Download pyinstaller, pyqt5 and mysqlclient from the repos
#+BEGIN_SRC sh
pyinstaller --onefile --windowed run.py
#+END_SRC
- Copy over run from dist/
- Remove build/ and dist/

* Database layout
- item ID (int, auto)
- taker (Student in GUI, varchar)
- old_taker (not implemented, varchar)
- giver (Werknemer in GUI, varchar)
- date (Datum in GUI, auto, DATE)
- taken (Status in GUI, boolean)

* Development
- Checkout the latest build at [[https://gitlab.com/valentijn/warenhuis][this gitlab repo.]]
- You should use [[https://www.python.org/dev/peps/pep-0008/][PEP 8]] for all code except the pyuic5 generated code.
- Use pylint or related tool to ensure code correctness
- Ensure you setup git correctly
- Write good [[http://chris.beams.io/posts/git-commit/][git commits!]]
- Make branches when working on the project so you don't accidently break the code if you have to stop
